// Express
const express = require('express');
const app = express();

const path = require('path');

// ENV
require('dotenv').config();

// CORS
var enableCORS = (req, res, next) => {
    // No producción!!!11!!!11one!!1!
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
    res.set('Access-Control-Allow-Headers', 'Content-Type, x-access-token, Sign-Type, Sign-Types, Sign-State, Sign-Data, Device-Id, Automatic-Login');
    res.set('Access-Control-Expose-Headers', 'Content-Type, x-access-token, Sign-Type, Sign-Types, Sign-State, Sign-Data, Device-Id, Automatic-Login');
    next();
};
app.use(enableCORS);

app.set('trust proxy', true);

// Body parser
app.use(express.json());

// MongoDB
require('./backend/mongoose.js');

// Port
const port = process.env.PORT || 3000;
app.listen(port);
console.log('API escuchando en el puerto ' + port);

// Security
const authentication = require('./backend/security/authentication');

// Controllers
const usersController = require('./backend/controllers/UserController');
const authController = require('./backend/controllers/AuthController');
const accountController = require('./backend/controllers/AccountController');
const loginActivityController = require('./backend/controllers/LoginActivityController');
const automaticLoginController = require('./backend/controllers/AutomaticLoginController');

// Front static content
app.use('/', express.static(path.join('build', 'default'), { index: 'index.html' }));

// Login
app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v2/logout', authController.logoutV2);

// User
app.post('/apitechu/v2/users', usersController.createUserV2);

// Account
app.get('/apitechu/v2/accounts', authentication.verifyAuth, accountController.getAccountsV2);
app.get('/apitechu/v2/accounts/:id', authentication.verifyAuth, accountController.getAccountV2);
app.post('/apitechu/v2/accounts', authentication.verifyAuth, authentication.verifySign, accountController.createAccountV2);
app.delete('/apitechu/v2/accounts/:id', authentication.verifyAuth, authentication.verifySign, accountController.removeAccountV2);

// Account Transactions
app.post('/apitechu/v2/accounts/:id/transaction', authentication.verifyAuth, accountController.makeTransactionV2);
app.post('/apitechu/v2/accounts/:id/transfer', authentication.verifyAuth, authentication.verifySign, accountController.makeTransferV2);

// Login Activity Transactions
app.get('/apitechu/v2/login/activity', authentication.verifyAuth, loginActivityController.getLoginActivityV2);

// Automatic Login
// app.get('/apitechu/v2/automaticlogin', authentication.verifyAuth, automaticLoginController.getAutomaticLoginV2);
app.post('/apitechu/v2/automaticlogin', authentication.verifyAuth, authentication.verifySign, automaticLoginController.createAutomaticLoginV2);
// app.delete('/apitechu/v2/automaticlogin/:id', authentication.verifyAuth, automaticLoginController.deleteAutomaticLoginV2);

// Redirect unknown requests
app.get('*', (req, res) => {
    res.redirect('/');
});
