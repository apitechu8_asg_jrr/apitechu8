const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const path = require('path');
const fs = require('fs');

var emailTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_USER, // la dirección de correo de gmail
        pass: process.env.EMAIL_PASSWORD // El password de gmail
    }
});

const remitente = '"Magic Bank"<apitechu8.asg.jrr@gmail.com>';

const sendEmail = (mailOptions) => {
    console.log('[NotificationManager]:sendEmail -> envío de email a ' + mailOptions.to);

    mailOptions.from = remitente;

    emailTransporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            console.log('[NotificationManager]:sendEmail--> ERROR: ' + err);
        } else {
            console.log('[NotificationManager]:sendEmail--> INFO: ' + info);
        }
    });
};

const sendEmailWithTemplate = (mailOptions, templatePath, replacements) => {
    console.log('[NotificationManager]:sendEmailWithTemplate -> envío de email a ' + mailOptions.to);

    const absoluteTemplatePath = path.join(__dirname, templatePath);
    console.log('[NotificationManager]:sendEmailWithTemplate -> template ' + absoluteTemplatePath);
    var html = fs.readFileSync(absoluteTemplatePath, 'utf8');

    var template = handlebars.compile(html);
    mailOptions.html = template(replacements);

    mailOptions.from = remitente;

    emailTransporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            console.log('[NotificationManager]:sendEmailWithTemplate--> ERROR: ' + err);
        } else {
            console.log('[NotificationManager]:sendEmailWithTemplate--> INFO: ' + info);
        }
    });
};

const sendEmailChallenge = (mailOptions, challenge) => {
    console.log('[NotificationManager]:sendEmailChallenge -> envío de email a ' + mailOptions.to);
    sendEmailWithTemplate(mailOptions, '/templates/email/EmailChallengeRequest.html', { challenge: challenge });
};

module.exports = {
    sendEmail: sendEmail,
    sendEmailWithTemplate: sendEmailWithTemplate,
    sendEmailChallenge: sendEmailChallenge
};
