const crypto = require('crypto-js');

const encryptStringAES = (data) => {
    var encrypted = crypto.AES.encrypt(JSON.stringify(data), process.env.CRYPTO_AES_SECRET).toString();
    return crypto.enc.Base64.stringify(crypto.enc.Utf8.parse(encrypted));
};

const decryptStringAES = (data) => {
    var base64Text = crypto.enc.Base64.parse(data.toString());
    var utf8Text = base64Text.toString(crypto.enc.Utf8);

    var decrypted = crypto.AES.decrypt(utf8Text, process.env.CRYPTO_AES_SECRET);
    return JSON.parse(decrypted.toString(crypto.enc.Utf8));
};

module.exports = {
    encryptStringAES: encryptStringAES,
    decryptStringAES: decryptStringAES
};
