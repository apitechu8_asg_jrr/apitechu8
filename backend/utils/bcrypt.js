const bcrypt = require('bcrypt');

// TODO SALT???

const hash = (data) => {
    console.log('Hashing data');
    return bcrypt.hashSync(data, 10);
};

const checkPassword = (sentPassword, userHashPassword) => {
    return bcrypt.compareSync(sentPassword, userHashPassword);
};

module.exports = {
    hash: hash,
    checkPassword: checkPassword
};
