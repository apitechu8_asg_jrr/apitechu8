const bcrypt = require('../utils/bcrypt.js');
const authentication = require('../security/authentication');
const validator = require('validator');
const crypto = require('../utils/crypto.js');
const notification = require('../utils/NotificationManager.js');

const User = require('../models/User');
const LoginActivity = require('../models/LoginActivity');
const AutomaticLogin = require('../models/AutomaticLogin');

const loginV2 = (req, res) => {
    console.log('POST /apitechu/v2/login');
    console.log('[AuthController]loginV2: ');

    switch (req.body.authenticationType) {
        case authentication.L1:
            _loginV2WithCredentials(req, res);
            break;
        case authentication.L2:
            _loginV2WithCustomerId(req, res);
            break;
        case authentication.L3:
            _loginV2WithAutomaticLogin(req, res);
            break;
        default:
            console.log('[AuthController]loginV2: unknown autenticationType' + req.body.authenticationType);
    }
};

const logoutV2 = (req, res) => {
    console.log('POST /apitechu/v2/logout');
    const result = {
        mensaje: 'Logout incorrecto'
    };

    const id = req.body.id;

    _findUserById(id).then(user => {
        console.log(user);
        if (!user) {
            res.status(400).send();
        } else if (user.logged) {
            result.mensaje = 'Logout correcto';
            res.send(result);
        } else {
            res.send(result);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

function _storeLoginActivity(req, user) {
    const loginActivity = new LoginActivity({
        user_id: user._id,
        userAgent: req.headers['user-agent'],
        ip: req.ip
    });

    return loginActivity.save().catch(err => {
        console.error(err);
    });
}

const _loginV2WithCredentials = (req, res) => {
    console.log('[AuthController]_loginV2WithCredentials');

    const result = {
    };

    const email = req.body.authenticationData.email.trim();
    const password = req.body.authenticationData.password.trim();

    if (!validator.isEmail(email) || validator.isEmpty(password)) {
        console.log('[AuthController]_loginV2WithCredentials: error in params validation ' + email + ' ' + password);
        res.status(400).send();
        return;
    }

    _findUserByEmail(email).then(user => {
        console.log(user);
        if (!user) {
            res.status(401).send();
        } else if (bcrypt.checkPassword(password, user.password)) {
            delete user.password;

            // result.user = user;
            result.jwtToken = authentication.generateTokenWithAuthType(user, req.body.authenticationType);

            if (req.body.authenticationData.remember_me) {
                result.customerId = user.customerId;
                result.customerName = user.first_name;
            }

            _storeLoginActivity(req, user);
            _securityAlertHelper(req, res, user, req.body.location);
            res.status(200).send(result);
        } else {
            res.status(401).send(result);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const _loginV2WithCustomerId = (req, res) => {
    console.log('[AuthController]_loginV2WithCustomerId');

    const result = {
    };

    const customerId = req.body.authenticationData.customerId;
    const password = req.body.authenticationData.password;

    if (!customerId || !password || validator.isEmpty(password)) {
        console.log('[AuthController]_loginV2WithCustomerId: error in params validation ' + customerId + ' ' + password);
        res.status(400).send();
        return;
    }

    _findUserByCustomerId(customerId).then(user => {
        console.log(user);
        if (!user) {
            res.status(401).send();
        } else if (bcrypt.checkPassword(password, user.password)) {
            delete user.password;
            result.idUsuario = user.id; // TODO
            result.jwtToken = authentication.generateTokenWithAuthType(user, req.body.authenticationType);
            result.customerId = user.customerId;
            result.customerName = user.first_name;

            _storeLoginActivity(req, user);
            _securityAlertHelper(req, res, user, req.body.location);
            res.status(200).send(result);
        } else {
            res.status(401).send(result);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const _loginV2WithAutomaticLogin = (req, res) => {
    console.log('[AuthController]_loginV2WithAutomaticLogin');

    const result = {
    };

    if (!req.body.authenticationData.automaticLogin) {
        res.status(400).send(result);
    }

    AutomaticLogin.findOne({ token: req.body.authenticationData.automaticLogin }).then(automaticLogin => {
        if (!automaticLogin) {
            res.status(401).send(result);
        }

        // TODO Comprobar expiración y tiempo máximo de vida del token 15 dias y 3 meses
        console.log(automaticLogin);

        _findUserById(automaticLogin.user_id).then(user => {
            console.log(user);
            if (!user) {
                res.status(401).send();
            } else {
                result.idUsuario = user.id; // TODO
                result.jwtToken = authentication.generateTokenWithAuthType(user, req.body.authenticationType);
                result.customerId = user.customerId;
                result.customerName = user.first_name;

                _storeLoginActivity(req, user);
                _securityAlertHelper(req, res, user, req.body.location);

                automaticLogin.lastLogin = new Date().toUTCString();
                automaticLogin.save((err) => {
                    console.log(err);
                    if (err) res.status(500).send();
                    res.setHeader('Automatic-Login', req.body.authenticationData.automaticLogin);
                    res.status(200).send(result);
                }
                );
            }
        }).catch(err => {
            console.error(err);
            res.status(500).send();
        });
    });
};

const _findUserById = (id) => {
    console.log('findUserById');
    return User.findById(id).then(user => Promise.resolve(user));
};

const _findUserByEmail = (email) => {
    console.log('findUserByEmail');
    return User.findOne({ email: email }).select('+password').lean().then(user => Promise.resolve(user));
};

const _findUserByCustomerId = (customerId) => {
    console.log('[AuthController]:_findUserByCustomerId');
    return User.findOne({ customerId: customerId }).select('+password').lean().then(user => Promise.resolve(user));
};

const _securityAlertHelper = (req, res, user, geo) => {
    var newDevice = false;

    if (req.headers['device-id']) {
        if (crypto.decryptStringAES(req.headers['device-id']).user !== user.email) {
            newDevice = true;
        }
    } else {
        newDevice = true;
    }

    console.log('geolocation para email ' + geo);

    if (newDevice) {
        const mailOptions = {
            to: user.email,
            subject: 'Alerta de seguridad: Inicio de sesión desde un nuevo dispositivo'
        };

        const replacements = {
            name: user.first_name,
            geo: (geo) || 'N/A',
            time: new Date().toUTCString(),
            userAgent: req.headers['user-agent']
        };

        notification.sendEmailWithTemplate(mailOptions, '/templates/email/NewDeviceSecurityAlertEmail.html', replacements);

        const data = {
            user: user.email
        };

        res.set('Device-Id', crypto.encryptStringAES(data));
    }
};

module.exports = {
    loginV2: loginV2,
    logoutV2: logoutV2
};
