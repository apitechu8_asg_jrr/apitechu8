const LoginActivity = require('../models/LoginActivity');

const getLoginActivityV2 = (req, res) => {
    console.log('POST /apitechu/v2/login/activity');
    console.log(req.authUserId);

    LoginActivity.find({ user_id: req.authUserId }).limit(10).sort({ date: -1 }).then((doc) => {
        console.log(doc);
        res.send(doc);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

module.exports = {
    getLoginActivityV2: getLoginActivityV2
};
