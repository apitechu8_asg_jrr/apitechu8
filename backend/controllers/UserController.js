const bcrypt = require('../utils/bcrypt.js');
const crypto = require('../utils/crypto.js');
const authentication = require('../security/authentication');
const validator = require('validator');

const User = require('../models/User');

const createUserV2 = (req, res) => {
    console.log('POST /apitechu/v2/users');
    console.log(req.body);

    if (!validator.isEmail(req.body.email) || validator.isEmpty(req.body.first_name.trim()) ||
        validator.isEmpty(req.body.last_name.trim()) || validator.isEmpty(req.body.password.trim())
        || req.body.password.trim().length < 6 || req.body.password.trim().length > 50) {
        res.status(400).send();
        return;
    }
    const customerId = crypto.encryptStringAES(req.body.email + req.body.first_name + req.body.last_name);

    const newUser = new User({
        email: req.body.email.trim(),
        first_name: req.body.first_name.trim(),
        last_name: req.body.last_name.trim(),
        password: bcrypt.hash(req.body.password.trim()),
        customerId: customerId
    });

    newUser.save().then(user => {
        const token = authentication.generateToken(user);

        if (req.body.remember_me) {
            res.set('Customer-Id', customerId);
        }

        res.status(200).send({ jwtToken: token });
    }).catch(err => {
        console.error(err);
        if (err.code === 11000) {
            res.status(409).send();
        } else {
            res.status(500).send();
        }
    });
};

module.exports = {
    createUserV2: createUserV2
};
