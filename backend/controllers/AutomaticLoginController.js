const AutomaticLogin = require('../models/AutomaticLogin');
const User = require('../models/User');

const crypto = require('../utils/crypto.js');
const notification = require('../utils/NotificationManager.js');

const getAutomaticLoginV2 = (req, res) => {
    console.log('GET /apitechu/v2/automaticlogin');
    console.log('[AutomaticLoginController]getAutomaticLoginV2: ');
};

const createAutomaticLoginV2 = (req, res) => {
    console.log('POST /apitechu/v2/automaticlogin');
    console.log('[AutomaticLoginController]createAutomaticLoginV2: ');

    const data = {
        user_id: req.authUserId
    };

    const token = crypto.encryptStringAES(data);

    const automaticLogin = new AutomaticLogin({
        user_id: req.authUserId,
        userAgent: req.headers['user-agent'],
        token: token
    });

    automaticLogin.save().catch(err => {
        console.error(err);
        res.status(500).send();
    });

    User.findById(req.authUserId).then(user => _sendAutomaticLoginSecurityAlert(user, req));

    res.setHeader('Automatic-Login', token);
    res.send();
};

const deleteAutomaticLoginV2 = (req, res) => {
    console.log('DELETE /apitechu/v2/automaticlogin');
    console.log('[AutomaticLoginController]deleteAutomaticLoginV2: ');
};

const _sendAutomaticLoginSecurityAlert = (user, req) => {
    const mailOptions = {
        to: user.email,
        subject: 'Alerta de seguridad: Activación de Acceso Automático desde un nuevo dispositivo'
    };

    const geo = req.body.location;

    console.log('[AutomaticLoginContoller]_sendAutomaticLoginSecurityAlert:  geolocation' + geo);

    const replacements = {
        name: user.first_name,
        geo: (geo) ? geo : 'N/A',
        time: new Date().toUTCString(),
        userAgent: req.headers['user-agent']
    };

    notification.sendEmailWithTemplate(mailOptions, '/templates/email/NewAutomaticLoginSecurityAlertEmail.html', replacements);
};

module.exports = {
    getAutomaticLoginV2: getAutomaticLoginV2,
    createAutomaticLoginV2: createAutomaticLoginV2,
    deleteAutomaticLoginV2: deleteAutomaticLoginV2
};
