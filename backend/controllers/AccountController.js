const mongoose = require('mongoose');
const Account = require('../models/Account');

const makeTransactionV2 = (req, res) => {
    console.log('POST /apitechu/v2/account/:id/transaction');
    console.log(req.params);

    const newTransaction = {
        amount: Number(req.body.amount),
        description: req.body.description,
        type: req.body.type,
        location: req.body.location
    };

    Account.findOne({ _id: mongoose.Types.ObjectId(req.params.id), user_id: req.authUserId }).then((doc) => {
        console.log(newTransaction);

        if (newTransaction.type === 'entry') {
            doc.balance += newTransaction.amount;
        } else if (newTransaction.type === 'refund') {
            doc.balance -= newTransaction.amount;
        }

        if (doc.balance < 0) {
            res.status(400).send();
            return Promise.reject();
        }

        doc.transactions.push(newTransaction);
        console.log(doc);

        return doc.save();
    }).then((doc) => {
        res.send(doc);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const makeTransferV2 = (req, res) => {
    console.log('POST /apitechu/v2/account/:id/transfer');
    console.log(req.params);

    let currentAccount;

    const transfer = {
        amount: Number(req.body.amount),
        description: req.body.description,
        to: req.body.to,
        location: req.body.location
    };

    Account.findOne({ _id: mongoose.Types.ObjectId(req.params.id), user_id: req.authUserId }).then((doc) => {
        console.log(doc);

        doc.balance -= transfer.amount;
        if (doc.balance < 0) {
            res.status(400).send();
            return Promise.reject();
        }

        doc.transactions.push({
            amount: transfer.amount,
            description: transfer.description,
            type: 'refund',
            location: transfer.location
        });

        return doc.save();
    }).then((doc) => {
        currentAccount = doc;
        return Account.findOne({ IBAN: transfer.to, user_id: req.authUserId });
    }).then((doc) => {
        console.log(doc);

        doc.balance += transfer.amount;
        doc.transactions.push({
            amount: transfer.amount,
            description: transfer.description,
            type: 'entry',
            location: transfer.location
        });

        return doc.save();
    }).then(() => {
        res.send(currentAccount);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const getAccountV2 = (req, res) => {
    console.log('GET /apitechu/v2/account/:id');

    Account.findOne({ _id: mongoose.Types.ObjectId(req.params.id), user_id: req.authUserId }).then((doc) => {
        res.send(doc);
    }).catch(err => {
        console.err(err);
        res.status(500).send();
    });
};

const getAccountsV2 = (req, res) => {
    console.log('GET /apitechu/v2/account');

    Account.find({ user_id: req.authUserId }).then((docs) => {
        res.send(docs);
    }).catch(err => {
        console.err(err);
        res.status(500).send();
    });
};

const createAccountV2 = (req, res) => {
    console.log('POST /apitechu/v2/account');

    const newAccount = new Account({
        user_id: req.authUserId,
        IBAN: generateRandomIBAN(),
        name: req.body.accountName,
        balance: 10,
        transactions: []
    });

    const newTransaction = {
        amount: 10,
        type: 'entry',
        description: 'Welcome gift for opening your account'
    };

    newAccount.transactions.push(newTransaction);

    newAccount.save().then(account => {
        res.send(account);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const removeAccountV2 = (req, res) => {
    console.log('DELETE /apitechu/v2/account/:id');

    const idAccount = parseInt(req.params.id);

    Account.findOneAndRemove({ _id: idAccount, user_id: req.authUserId }).then(() => {
        res.status(201).send();
    }).catch(err => {
        console.err(err);
        res.status(500).send();
    });
};

function generateRandomIBAN() {
    const numbers = '0123456789';
    let cc = '';
    for (let i = 0; i < 18; i++) {
        cc += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }

    return 'ES' + cc.substr(0, 2) + '0182' + cc.substr(2);
};

module.exports = {
    getAccountV2: getAccountV2,
    getAccountsV2: getAccountsV2,
    createAccountV2: createAccountV2,
    removeAccountV2: removeAccountV2,
    makeTransactionV2: makeTransactionV2,
    makeTransferV2: makeTransferV2
};
