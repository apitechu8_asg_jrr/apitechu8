const jwt = require('jsonwebtoken');
const crypto = require('../utils/crypto.js');
const bcrypt = require('../utils/bcrypt.js');
const notification = require('../utils/NotificationManager.js');

const User = require('../models/User');

const JWT_EXPIRE_TIME_SECONDS = 60 * 60; // 1h
const OTP_EXPIRE_TIME_MILLISECONDS = 5 * 60 * 1000; // 5m

// ---- Login Labels ----
const L1 = 'L1'; // Email + Password
const L2 = 'L2'; // CustomerIdToken + Password
const L3 = 'L3'; // AutomaticLoginToken

// ---- Sign Labels -----
const S1 = 'S1'; // OTP-Mail
// const S2 = 'S2'; // OTP-SMS
// const S3 = 'S3'; // T-OTP
const S4 = 'S4'; // Password

const authList = [L1, L2, L3]; // [L1, L2, L3]
const signList = [S1, S4]; // [S1, S2, S3, S4]

const stateModel = {
    L1: [S1], // L1: [S1, S2, S3]
    L2: [S1], // L2: [S1, S2, S3]
    L3: [S4]
};

const generateToken = (user) => {
    return jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        expiresIn: JWT_EXPIRE_TIME_SECONDS
    });
};

const generateTokenWithAuthType = (user, authenticationType) => {
    return jwt.sign({ id: user._id, authenticationType: authenticationType }, process.env.JWT_SECRET, {
        expiresIn: JWT_EXPIRE_TIME_SECONDS
    });
};

const verifyAuth = (req, res, next) => {
    var token = req.headers['x-access-token'];
    if (!token) {
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            if (err.name === 'TokenExpiredError') {
                return res.status(401).send({ auth: false, message: 'Session expired!' });
            } else {
                return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            }
        }

        req.authUserId = decoded.id;
        req.authType = decoded.authenticationType;
        next();
    });
};

const verifySign = (req, res, next) => {
    console.log('[Authentication] verifySign: Start method');

    var signType = req.headers['sign-type'];
    var signState = req.headers['sign-state'];
    var signData = req.headers['sign-data'];

    if (signType) {
        if (signState && signData) {
            stage3(req, res, next);
        } else {
            stage2(req, res, next);
        }
    } else {
        stage1(req, res, next);
    }
};

// Primer paso donde se le informa al frontal que este servicio requiere 2FA. Y se le devuelve una lista
// de mecanismos de firma permitidos (cabecera Sign-Types) en base al autentication type utilizado
const stage1 = (req, res, next) => {
    console.log('[Authentication] verifySign -> State 1: 2FA required for this service');
    res.set('Sign-Types', stateModel[req.authType]);

    return res.status(403).send({ sign: false, message: 'State 1: 2FA required for this service' });
};

// El frontal especifica el mecanismo de firma que desea utilizar (cabecera Sign-Type) de la lista devuelta en el Stage 1
const stage2 = (req, res, next) => {
    console.log('[Authentication] verifySign -> State 2: start');
    var signType = req.headers['sign-type'];
    var authType = req.authType;

    res.set('Sign-Types', stateModel[authType]);

    // ver que el mecanismo de firma existe
    if (signList.indexOf(signType) === -1) {
        console.log('[Authentication] verifySign -> State 2: unknown Sign Type: ' + signType);
        return res.status(500).send({ message: 'State 2: Unknown Sign Type' });
    }

    const otp = _generateRandomOTP();

    switch (signType) {
        case S1:
            _findUserById(req.authUserId).then(user => {
                if (!user) {
                    return res.status(404).send({ message: 'State 2: Unexpected error. User not found' });
                } else {
                    // enviamos por email
                    _sendOTPByEmail(otp, user.email, user.first_name);
                }
            }).catch(err => {
                console.error(err);
                res.status(500).send({ message: 'State 2: Unexpected error' });
            });
            break;
        // case S2:
        //     userController.findUserById(req.authUserId).then(user => {
        //         if (!user) {
        //             return res.status(404).send({ message: 'State 2: Unexpected error. User not found' });
        //         } else {
        //             // enviamos por SMS
        //             _sendOTPBySMS(otp, user.phone);
        //         }
        //     }).catch(err => {
        //         console.error(err);
        //         res.status(500).send({ message: 'State 2: Unexpected error' });
        //     })
        //     break;
        // case S3:
        case S4:
            // No hacemos nada aquí
            break;
        default:
            // No se debería de dar nunca, ya que lo hemos controlado en el primer if de este método
            console.log('[Authentication] verifySign -> State 2: Unexpected error in select auth type');
            return res.status(500).send({ message: 'State 2: Unexpected error in select auth type' });
    }

    res.set('Sign-State', _generateSignState(req, otp, signType));

    return res.status(403).send({ sign: false, message: 'State 2: finish OK' });
};

const stage3 = (req, res, next) => {
    var signType = req.headers['sign-type'];
    var authType = req.authType;

    console.log('[Authentication] verifySign -> State 3: ' + signType);

    // ver que el mecanismo de firma que viene en sign type esta en la lista  de admitidos
    if (stateModel[authType].indexOf(signType) === -1) {
        console.log('[Authentication] verifySign -> State 3: Sing Type ' + signType + ' not permited for ' + authType);
        res.set('Sign-Types', stateModel[authType]);
        return res.status(500).send({ message: 'Sing Type ' + signType + ' not permited for ' + authType });
    }

    // ver que la otp que llega en sign data es correcta para esta operación
    _validateSignState(req, res, next);

    // if (_validateSignState(req, signType, signState, signData)) {
    //     console.log('[Authentication] verifySign -> State 3: Sign OK');
    //     next();
    // } else {
    //     console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
    //     return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
    // };
};

const _findUserById = (id) => {
    console.log('findUserById');
    return User.findById(id).then(user => Promise.resolve(user));
};

const _generateRandomOTP = () => {
    return Math.floor(Math.random() * 900000) + 100000;
};

const _sendOTPByEmail = (otp, to, name) => {
    const mailOptions = {
        to: to,
        subject: 'Esta es la OTP para firmar la operación'
    };

    const replacements = {
        operation: 'Esta cadena respresenta la operación que estoy firmando',
        otp: otp,
        name: name
    };

    notification.sendEmailWithTemplate(mailOptions, '/templates/email/OTPEmail.html', replacements);
};

// const _sendOTPBySMS = (otp, to) => {
//     // TODO
// };

const _generateSignState = (req, otp, signType) => {
    // meter dentro del state tiempo, para tener caducidad en la firma
    var data;

    switch (signType) {
        case S1:
            // case S2:
            data = {
                'x-access-token': req.headers['x-access-token'],
                'sign-type': req.headers['sign-type'],
                'user-agent': req.headers['user-agent'],
                'host': req.headers['host'],
                'body': JSON.stringify(req.body),
                'otp': otp,
                'timestamp': new Date().getTime()
            };
            break;
        // 3 y 4 no metemos otp dentro del sing state
        // case S3:
        case S4:
            data = {
                'x-access-token': req.headers['x-access-token'],
                'sign-type': req.headers['sign-type'],
                'user-agent': req.headers['user-agent'],
                'host': req.headers['host'],
                'body': JSON.stringify(req.body),
                'timestamp': new Date().getTime()
            };
            break;
    }

    return crypto.encryptStringAES(data);
};

const _validateSignState = (req, res, next) => {
    var signType = req.headers['sign-type'];
    var signState = req.headers['sign-state'];
    var signData = req.headers['sign-data'];

    var decode = crypto.decryptStringAES(signState);

    var result;

    switch (signType) {
        case S1:
            // case S2:
            result = (
                decode['x-access-token'] == req.headers['x-access-token'] &&
                decode['sign-type'] == req.headers['sign-type'] &&
                decode['user-agent'] == req.headers['user-agent'] &&
                decode['host'] == req.headers['host'] &&
                decode['body'] == JSON.stringify(req.body) &&
                decode['otp'] == signData &&
                (new Date().getTime() - new Date(decode['timestamp']).getTime()) <= OTP_EXPIRE_TIME_MILLISECONDS
            );

            if (result) {
                console.log('[Authentication] verifySign -> State 3: Sign OK');
                next();
            } else {
                console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
                return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
            }

            break;
        // 3 y 4 no metemos otp dentro del sing state
        // PERO HAY QUE VALIDAR LA OTP
        // case S3:
        case S4:
            result = (
                decode['x-access-token'] == req.headers['x-access-token'] &&
                decode['sign-type'] == req.headers['sign-type'] &&
                decode['user-agent'] == req.headers['user-agent'] &&
                decode['host'] == req.headers['host'] &&
                decode['body'] == JSON.stringify(req.body) &&
                (new Date().getTime() - new Date(decode['timestamp']).getTime()) <= OTP_EXPIRE_TIME_MILLISECONDS
            );
            if (result) {
                User.findById(req.authUserId).select('+password').then(user => {
                    if (!user) {
                        console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
                        return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
                    } else if (bcrypt.checkPassword(signData, user.password)) {
                        console.log('[Authentication] verifySign -> State 3: Sign OK');
                        next();
                    } else {
                        console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
                        return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
                    }
                }).catch(() => {
                    console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
                    return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
                });
            } else {
                console.log('[Authentication] verifySign -> State 3: Sing ERROR ');
                return res.status(403).send({ sign: false, message: 'State 3: Sign Failed!' });
            }

            break;
    }
};

module.exports = {
    L1: L1,
    L2: L2,
    L3: L3,
    generateToken: generateToken,
    generateTokenWithAuthType: generateTokenWithAuthType,
    verifyAuth: verifyAuth,
    verifySign: verifySign
};
