var mongoose = require('mongoose');

var mongoDB = process.env.MONGODB_URL;
mongoose.connect(mongoDB);

mongoose.Promise = global.Promise;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
