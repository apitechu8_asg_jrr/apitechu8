const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-dompurify');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: { type: String, require: true, unique: true },
    first_name: { type: String, require: true },
    last_name: { type: String, require: true },
    password: { type: String, require: true, select: false },
    customerId: { type: String, require: true }
}, { timestamps: false, collection: 'user' });

UserSchema.plugin(sanitizerPlugin, {
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        SAFE_FOR_JQUERY: true,
        SAFE_FOR_TEMPLATES: true,
        ALLOWED_TAGS: []
    }
});

module.exports = mongoose.model('User', UserSchema);
