const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-dompurify');

const Schema = mongoose.Schema;

const AccountSchema = new Schema({
    user_id: { type: mongoose.ObjectId, require: true },
    IBAN: { type: String, require: true, unique: true },
    name: { type: String, require: true },
    balance: { type: Number, default: 0 },
    currency: { type: String, default: 'EUR' },
    transactions: [{
        amount: { type: Number, require: true },
        type: { type: String, require: true, enum: ['entry', 'refund'] },
        description: { type: String },
        location: {
            latitude: { type: Number },
            longitude: { type: Number }
        }
    }]
}, { timestamps: true });

AccountSchema.plugin(sanitizerPlugin, {
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        SAFE_FOR_JQUERY: true,
        SAFE_FOR_TEMPLATES: true,
        ALLOWED_TAGS: []
    }
});

module.exports = mongoose.model('Account', AccountSchema);
