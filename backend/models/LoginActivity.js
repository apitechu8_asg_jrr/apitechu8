const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-dompurify');

const Schema = mongoose.Schema;

const LoginActivitySchema = new Schema({
    user_id: { type: mongoose.ObjectId, require: true },
    userAgent: { type: String, require: true },
    date: { type: Date, default: Date.now },
    ip: { type: String }
}, { timestamps: false });

LoginActivitySchema.plugin(sanitizerPlugin, {
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        SAFE_FOR_JQUERY: true,
        SAFE_FOR_TEMPLATES: true,
        ALLOWED_TAGS: []
    }
});

module.exports = mongoose.model('LoginActivity', LoginActivitySchema);
