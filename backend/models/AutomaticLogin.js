const mongoose = require('mongoose');
const sanitizerPlugin = require('mongoose-dompurify');

const Schema = mongoose.Schema;

const AutomaticLoginSchema = new Schema({
    user_id: { type: mongoose.ObjectId, require: true },
    token: { type: String, require: true },
    userAgent: { type: String, require: true },
    createTime: { type: Date, default: Date.now, require: true },
    lastLogin: { type: String, require: true }
}, { timestamps: false });

AutomaticLoginSchema.plugin(sanitizerPlugin, {
    skip: [],
    encodeHtmlEntities: false,
    sanitizer: {
        SAFE_FOR_JQUERY: true,
        SAFE_FOR_TEMPLATES: true,
        ALLOWED_TAGS: []
    }
});

module.exports = mongoose.model('AutomaticLogin', AutomaticLoginSchema);
